package com.home.jdbc.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.home.jdbc.entity.Flat;
import com.home.jdbc.entity.Person;
import com.home.jdbc.helpers.JDBCHelper;
import com.home.jdbc.helpers.NoConnectionException;

/**
 * SQL:
 * create table PERSON (ID bigint not null, EMAIL varchar(255), FIRST_NAME varchar(255), JOINED_DATE date, LAST_NAME varchar(255), primary key (id))
 * This class is used to test CRUD operations on DB.
 *
 * @author preetham
 */
public class JDBCDaoImpl {
    public static final String INSERT_SQL_QUERY = "INSERT INTO FLAT(ID,STATE,SIZE) VALUES(?,?,?)";
    public static final String UPDATE_SQL_QUERY = "UPDATE FLAT SET STATE=? WHERE ID=?";
    public static final String SELECT_SQL_QUERY = "SELECT ID,STATE,SIZE FROM FLAT WHERE ID=?";
    public static final String SELECT_ALL_SQL_QUERY = "SELECT ID,STATE,SIZE FROM FLAT";
    public static final String DELETE_SQL_QUERY = "DELETE FROM FLAT WHERE ID=?";
    public static final String DELETE_ALL_SQL_QUERY = "DELETE FROM FLAT";
    public static final String ID = "ID";
    public static final String STATE = "STATE";
    public static final String SIZE = "SIZE";

    public static void main(String[] args) {


        try {
            JDBCHelper.testConnection();
        } catch (NoConnectionException e) {
            e.printStackTrace();
            System.exit(1);
        }


        Flat flat = new Flat(7,"good",25);
        Flat flat2 = new Flat(9,"good",50);

        try {
        // Create
         //insertFlat(flat);
         //insertFlat(flat2);

          System.out.println("Flat got inserted sucessfully. This is \"C\" of CRUD ");
          System.out.println();
          System.out.println("--------------------------------------------------------------------------------------");
         //Read(get all)
               List<Flat> flats = retrieveFlats();
             System.out.println("Retrived all persons from DB.This is \"R\" of CRUD ");
             for (Flat f : flats) {
                 System.out.println(f);
             }
             System.out.println();
             System.out.println("--------------------------------------------------------------------------------------");

         //Update
             flat2.setState("Luxury");
             updateFlatState(flat2);
             System.out.println("Updated the state of flat2. This is \"U\" of CRUD ");
            System.out.println();
             System.out.println("--------------------------------------------------------------------------------------");

             //Read(get one )
            Flat flatRetrieved = retrieveFlat(7);
            System.out.println("Retrived flat with id 7 from DB " + flatRetrieved);
           System.out.println();
           System.out.println("--------------------------------------------------------------------------------------");

        // Delete
              deleteFlat(10);
             System.out.println("Deleted person2 from DB.This is \"D\" of CRUD ");
            System.out.println();
            System.out.println("--------------------------------------------------------------------------------------");

            //Read(get all)
            List<Flat> tempFlats = retrieveFlats();
            System.out.println("Retrieved all persons from DB. Notice flat with id 10 is not present");
            for (Flat f : tempFlats) {
                System.out.println(f);
            }
            System.out.println();
            System.out.println("--------------------------------------------------------------------------------------");

            // Delete
            deleteAllRecords();
            System.out.println("Deleted all records");

        } catch (SQLException e) {
            System.out.println("Exception occured " + e.getMessage());
        } catch (Exception e) {
            System.out.println("Exception occured " + e.getMessage());
        }


    }

    private static void insertFlat(Flat flat) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = JDBCHelper.getConnection();
            if (con == null) {
                System.out.println("Error getting the connection. Please check if the DB server is running");
                return;
            }
            con.setAutoCommit(false);
            ps = con.prepareStatement(INSERT_SQL_QUERY);
            ps.setLong(1, flat.getId());
            ps.setString(2, flat.getState());
            ps.setInt(3, flat.getSize());
            ps.execute();
            System.out.println("insertFlat => " + ps.toString());
            con.commit();
        } catch (SQLException e) {
            try {
                if (con != null) {
                    con.rollback();
                }
            } catch (SQLException e1) {
                throw e1;
            }
            throw e;
        } finally {
            try {
                JDBCHelper.closePreparedStatement(ps);
                JDBCHelper.closeConnection(con);
            } catch (SQLException e) {
                throw e;
            }
        }
    }

    private static List<Flat> retrieveFlats() throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Flat> flats = new ArrayList<Flat>();
        try {
            con = JDBCHelper.getConnection();
            if (con == null) {
                System.out.println("Error getting the connection. Please check if the DB server is running");
                return flats;
            }
            ps = con.prepareStatement(SELECT_ALL_SQL_QUERY);
            rs = ps.executeQuery();
            System.out.println("retrivePersons => " + ps.toString());
            while (rs.next()) {
                Flat flat = new Flat();
                flat.setId(rs.getLong(ID));
                flat.setState(rs.getString(STATE));
                flat.setSize(rs.getInt(SIZE));
                flats.add(flat);
            }
        } catch (SQLException e) {
            throw e;
        } finally {
            try {
                JDBCHelper.closeResultSet(rs);
                JDBCHelper.closePreparedStatement(ps);
                JDBCHelper.closeConnection(con);
            } catch (SQLException e) {
                throw e;
            }
        }
        return flats;
    }

    private static void updateFlatState(Flat flat) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = JDBCHelper.getConnection();
            if (con == null) {
                System.out.println("Error getting the connection. Please check if the DB server is running");
                return;
            }
            con.setAutoCommit(false);
            ps = con.prepareStatement(UPDATE_SQL_QUERY);
            ps.setString(1, flat.getState());
            ps.setLong(2, flat.getId());
            ps.execute();
            System.out.println("updateFlatState => " + ps.toString());
            con.commit();
        } catch (SQLException e) {
            try {
                if (con != null) {
                    con.rollback();
                    throw e;
                }
            } catch (SQLException e1) {
                throw e1;
            }
        } finally {
            try {
                JDBCHelper.closePreparedStatement(ps);
                JDBCHelper.closeConnection(con);
            } catch (SQLException e) {
                throw e;
            }
        }
    }

    private static Flat retrieveFlat(long id) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Flat flat = new Flat();
        try {
            con = JDBCHelper.getConnection();
            if (con == null) {
                System.out.println("Error getting the connection. Please check if the DB server is running");
                return flat;
            }
            ps = con.prepareStatement(SELECT_SQL_QUERY);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            System.out.println("retriveFlat => " + ps.toString());
            while (rs.next()) {
                flat.setId(rs.getLong(ID));
                flat.setState(rs.getString(STATE));
                flat.setSize(rs.getInt(SIZE));
            }
        } catch (SQLException e) {
            throw e;
        } finally {
            try {
                JDBCHelper.closeResultSet(rs);
                JDBCHelper.closePreparedStatement(ps);
                JDBCHelper.closeConnection(con);
            } catch (SQLException e) {
                throw e;
            }
        }
        return flat;
    }

    private static void deleteFlat(int id) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = JDBCHelper.getConnection();
            ps = con.prepareStatement(DELETE_SQL_QUERY);
            ps.setLong(1, id);
            ps.execute();
            System.out.println("deleteFlat => " + ps.toString());
        } catch (SQLException e) {
            throw e;
        } finally {
            try {
                JDBCHelper.closePreparedStatement(ps);
                JDBCHelper.closeConnection(con);
            } catch (SQLException e) {
                throw e;
            }
        }
    }

    private static void deleteAllRecords() throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = JDBCHelper.getConnection();
            if (con == null) {
                System.out.println("Error getting the connection. Please check if the DB server is running");
                return;
            }
            ps = con.prepareStatement(DELETE_ALL_SQL_QUERY);
            ps.execute();
            System.out.println("deleteAllRecords => " + ps.toString());
        } catch (SQLException e) {
            throw e;
        } finally {
            try {
                JDBCHelper.closePreparedStatement(ps);
                JDBCHelper.closeConnection(con);
            } catch (SQLException e) {
                throw e;
            }
        }
    }

}