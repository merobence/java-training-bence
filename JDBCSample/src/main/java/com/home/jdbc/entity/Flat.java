package com.home.jdbc.entity;

public class Flat {
    private long id;
    private String state;
    private Integer size;

    public Flat() {
    }

    public Flat(long id, String state, Integer size) {
        this.id = id;
        this.state = state;
        this.size = size;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    public Integer getSize() {
        return size;
    }
    public void setSize(Integer size) {
        this.size = size;
    }
}



