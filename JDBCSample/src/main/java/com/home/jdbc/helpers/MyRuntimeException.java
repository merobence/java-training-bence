package com.home.jdbc.helpers;

public class MyRuntimeException extends RuntimeException {
    public MyRuntimeException() {
        super("This is my RunTimeException.");
    }
}
