package com.home.jdbc.helpers;

public class NoConnectionException extends Exception {
    public NoConnectionException() {
        super("No connection to DB.");
    }
}
